# 每日一学 思考题
### Go中的定时任务3
[场景](https://studygolang.com/topics/9585)  

[对于场景一，如果希望运行 20 秒后停止，该如何实现？](https://gitea.com/4fun/study_everyday/src/branch/master/timer/stop_ticker_after_20_sec.go)  

[对于场景二，如果希望白天工作，晚上不工作（早7点到晚20点），如何实现？](https://gitea.com/4fun/study_everyday/src/branch/master/timer/work_between7and20.go)

###  如何正确的使用 unsafe.Pointer 和 uintptr  
[场景](https://studygolang.com/topics/9751)  

[有一个自定义类型 type MyString string，如何将 []MyString 和 []string 相互转换，并且共用底层元素？](https://gitea.com/4fun/study_everyday/src/branch/master/unsafe-pointer/mystringSlice2StringSlice.go)  

[如何实现 string 和 []byte 的转换，不会复制底层字节系列？](https://gitea.com/4fun/study_everyday/src/branch/master/unsafe-pointer/mystringSlice2StringSlice.go)

# 知识星球作业题  
[用Go语言编写一个文件下载程序，核心是要控制下载速度不超过 20k/s ，实现方式不限！](https://gitea.com/4fun/study_everyday/src/branch/master/homework/file_downloader.go)
