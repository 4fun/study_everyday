package timer

import (
	"fmt"
	"math/rand"
	"strings"
	"sync"
	"time"
)

const chars = "abcdefghijklmnopqrstuvwxyz"

type Person struct {
	Name string
}

func IGoHome() {
	// 为了方便测试，30 分钟改为 10秒
	ticker := time.NewTicker(10 * time.Second)
	defer ticker.Stop()

	maxPersons := make([]*Person, 0, 7)

	var stop sync.Once
	var start sync.Once

	for {
		// 判断是否在 7 点到 20 点之间
		if !betweenWorkTime() {
			stop.Do(printNotWork)
			continue
		}
		start.Do(printWork)
		person, ok := NewPersonComming()
		if ok {
			maxPersons = append(maxPersons, person)
		} else {
			time.Sleep(2e9)
		}

		select {
		case <-ticker.C:
			//  时间已到，发车
			CarMoving(maxPersons)
		default:
			// 时间未到，判断是否坐满
			if len(maxPersons) == 7 {
				CarMoving(maxPersons)

				maxPersons = make([]*Person, 0, 7)
			}
		}
	}
}

// betweenWorkTime returns if it is between start time and end time.
func betweenWorkTime() bool {
	now := time.Now()
	start := time.Date(now.Year(), now.Month(), now.Day(), 7, 0, 0, 0, now.Location())
	stop := time.Date(now.Year(), now.Month(), now.Day(), 20, 0, 0, 0, now.Location())
	if time.Now().Before(start) {
		return false
	}
	if time.Now().After(stop) {
		return false
	}
	return true
}

func printNotWork() {
	fmt.Println("正在歇班，请耐心等待～")
}

func printWork() {
	fmt.Println("开工了！")
}

func NewPersonComming() (*Person, bool) {
	if rand.Intn(4) == 0 {
		return nil, false
	}
	var builder strings.Builder

	for i := 0; i < 4; i++ {
		builder.WriteByte(chars[rand.Intn(26)])
	}

	fmt.Println(builder.String(), " is coming!")
	return &Person{Name: builder.String()}, true
}

func CarMoving(maxPersons []*Person) {
	fmt.Println("==================")
	fmt.Println("The car is moving, is full or not？", len(maxPersons) == 7)

	fmt.Print("They are: ")
	for _, person := range maxPersons {
		fmt.Print(person.Name, "\t")
	}

	fmt.Println("\n==================")
}
