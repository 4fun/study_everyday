package timer

import (
	"log"
	"time"
)

func SimpleTicker() {
	ticker := time.NewTicker(2e9)
	defer ticker.Stop()
	// 添加一个channel用来实现定时效果
	stop := make(chan bool)
	// 为了测试方便，改为4秒。
	time.AfterFunc(time.Second*4, func() {
		stop <- true
	})
	for {
		select {
		case <-ticker.C:
			log.Println("hello study golang")
		case <-stop:
			return
		default:
		}
	}
}
