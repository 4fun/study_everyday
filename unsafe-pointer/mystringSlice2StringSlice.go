package unsafe_pointer

import (
	"unsafe"
)

type MyString string

// 有一个自定义类型 type MyString string，如何将 []MyString 和 []string 相互转换，并且共用底层元素？
func MyStringSlice2StringSlice(ms []MyString) []string {
	return *(*[]string)(unsafe.Pointer(&ms))
}
func StringSlice2MyStringSlice(ss []string) []MyString {
	return *(*[]MyString)(unsafe.Pointer(&ss))
}

// 如何实现 string 和 []byte 的转换，不会复制底层字节系列？
func ByteSlice2String(bs []byte) string {
	return *(*string)(unsafe.Pointer(&bs))
}
func String2ByteSlice(s string) []byte {
	return *(*[]byte)(unsafe.Pointer(&s))
}
