package util

import (
	"log"
)

// HandleError calls os.Exit(1) when err is not nil
func HandleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// PanicError panics if err is not nil
func PanicError(err error) {
	if err != nil {
		panic(err)
	}
}
