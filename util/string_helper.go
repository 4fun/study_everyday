package util

// Substring returns the substring of src from index start to end.
func Substring(src string, start, end int) string {
	r := []rune(src)
	len := len(r)
	if start < 0 || end > len || start > end {
		return ""
	}
	if start == 0 && end == len {
		return src
	}
	return string(r[start:end])
}
