package main

import (
	"fmt"
	"strings"

	unsafe_pointer "gitea.com/4fun/study_everyday/unsafe-pointer"
)

func main() {
	// string slice 2 MyString slice
	ss := []string{"go", "land"}
	fmt.Printf("Before Convert: %T--%q\n", ss, ss)
	ms := unsafe_pointer.StringSlice2MyStringSlice(ss)
	fmt.Printf("After Convert: %T--%q\n", ms, ms)
	// MyString slice 2 string slice
	ms2 := []unsafe_pointer.MyString{"go", "land"}
	fmt.Printf("Before Convert: %T--%q\n", ms2, ms2)
	ss2 := unsafe_pointer.MyStringSlice2StringSlice(ms2)
	fmt.Printf("After Convert: %T--%q\n", ss2, ss2)
	// string 2 []byte
	str := strings.Join([]string{"Go", "land"}, "")
	fmt.Printf("Before Convert: %T--%q\n", str, str)
	bs := unsafe_pointer.String2ByteSlice(str)
	fmt.Printf("After Convert: %T--%q\n", bs, bs)
	bs[2] = 'k'
	// []byte 2 string
	bs2 := []byte{'g', 'o', 'l', 'a', 'n', 'd'}
	fmt.Printf("Before Convert: %T--%q\n", bs2, bs2)
	str2 := unsafe_pointer.ByteSlice2String(bs2)
	fmt.Printf("After Convert: %T--%q\n", str2, str2)

	// rand.Seed(time.Now().UnixNano())
	// timer.SimpleTicker()
	// timer.IGoHome()
	// d := homework.FileDownloader{URL: "http://d18.aixdzs.com/141/141769/141769txt.zip", Speed: 1}
	// d.Download()
}
