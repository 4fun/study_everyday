package homework

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"
	"time"

	"gitea.com/4fun/study_everyday/util"
)

type FileDownloader struct {
	URL string
}

func (fd *FileDownloader) Download() {
	// 解析 url 和下载的文件名称
	url, err := url.ParseRequestURI(fd.URL)
	util.HandleError(err)
	filename := path.Base(url.Path)
	if filename == "" {
		filename = "download"
	}
	// 请求获取下载总量
	c := http.Client{}
	resp, err := c.Get(fd.URL)
	defer resp.Body.Close()
	total := resp.ContentLength
	util.HandleError(err)
	// 给出下载提示
	tf := time.Now().Format("2006-01-02 15:04:05")
	fmt.Printf("[%s] to download \"%s--%dKB\"\n", tf, filename, total)
	// 新建文件用于存储
	file, err := os.Create(filename)
	util.HandleError(err)
	// 限速下载
	bufr := bufio.NewReader(resp.Body)
	bufw := bufio.NewWriter(file)
	buffer := make([]byte, 20*1024) // 通过buffer的容量控制下载速度
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	written := 0
	for {
		select {
		case <-ticker.C:
			nr, err := bufr.Read(buffer)
			fmt.Println("nr= ", nr)
			if nr > 0 {
				nw, erw := bufw.Write(buffer[:nr])
				fmt.Println("nw= ", nw)
				util.HandleError(erw)
				if nr != nw {
					err = io.ErrShortWrite
					util.HandleError(err)
					break
				}
				written += nw
			}
			if err == io.EOF {
				fmt.Println("下载完成")
				return
			}
			rate := float64(written) / float64(total)
			fmt.Printf("已下载 %dB(%.2f%%)\n", written, rate*100)
		}
	}
}
